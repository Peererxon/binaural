$(document).ready(function(){
  $('.slide-slick').slick({
    infinite		: true,
    autoplay    : true,
	  speed 			: 1500,
	  fade 			  : true,
	  arrows      : false,
  });
});

$('.slider2').slick({
    infinite      : true,
    autoplay      : true,
    autoplaySpeed : 2000,
    speed         : 1500,
    fade          : true,
    arrows        : true,
});

$(document).ready(function(){
  $('#slider-horizontal').slick({
    infinite		: true,
    autoplay    : true,
	speed 			  : 1500,
	vertical		  :true,
	slidesToShow	:3,
	slidesToScroll:2,
  //pauseOnFocus  :true
  });
});